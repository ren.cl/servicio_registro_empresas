import flask_admin as admin
import flask_login as login
from app import app
from .login_form import LoginForm
from .registration_form import RegistrationForm
from .filters import FilterLastNameBrown
from app.models.user import User
from app.models.user_info import UserInfo

from flask_admin import helpers, expose
from flask_admin.contrib.sqla.filters import FilterEqual
from flask import url_for, redirect, request
from werkzeug.security import generate_password_hash

from flask_security import roles_required

from app.models import db

# Customized User model admin
inline_form_options = {
    'form_label': "Info item",
    'form_columns': ['id', 'key', 'value'],
    'form_args': None,
    'form_extra_fields': None,
}


# Create customized index view class that handles login & registration
class MyAdminIndexView(admin.AdminIndexView):

    @expose('/')
    def index(self):
        if not login.current_user.is_authenticated:
            return redirect(url_for('.login_view'))
        return super(MyAdminIndexView, self).index()

    @expose('/login/', methods=('GET', 'POST'))
    def login_view(self):
        # handle user login
        form = LoginForm(request.form)
        if helpers.validate_form_on_submit(form):
            user = form.get_user()
            login.login_user(user)

        if login.current_user.is_authenticated:
            return redirect(url_for('.index'))
        link = '<p>¿No tienes una cuenta? <a href="' + url_for('.register_view') + '">Click para registro.</a></p>'
        self._template_args['form'] = form
        self._template_args['link'] = link
        return super(MyAdminIndexView, self).index()

    @expose('/register/', methods=('GET', 'POST'))
    def register_view(self):
        form = RegistrationForm(request.form)
        if helpers.validate_form_on_submit(form):
            user = User()

            form.populate_obj(user)
            # we hash the users password to avoid saving it as plaintext in the db,
            # remove to use plain text:
            user.password = generate_password_hash(form.password.data)

            db.session.add(user)
            db.session.commit()

            login.login_user(user)
            return redirect(url_for('.index'))
        link = '<p>¿Ya tienes una cuenta? <a href="' + url_for('.login_view') + '">Click para entrar.</a></p>'
        self._template_args['form'] = form
        self._template_args['link'] = link
        return super(MyAdminIndexView, self).index()

    @expose('/logout/')
    def logout_view(self):
        login.logout_user()
        return redirect(url_for('.index'))

    action_disallowed_list = ['delete', ]
    column_display_pk = True
    column_list = [
        'id',
        'last_name',
        'first_name',
        'email',
        #'pets',
    ]
    column_default_sort = [('last_name', False), ('first_name', False)]  # sort on multiple columns

    # custom filter: each filter in the list is a filter operation (equals, not equals, etc)
    # filters with the same name will appear as operations under the same filter
    column_filters = [
        FilterEqual(column=User.last_name, name='Last Name'),
        FilterLastNameBrown(column=User.last_name, name='Last Name',
                            options=(('1', 'Yes'), ('0', 'No')))
    ]
    inline_models = [(UserInfo, inline_form_options), ]

    # setup create & edit forms so that only 'available' pets can be selected
    def create_form(self):
        return self._use_filtered_parent(
            super(MyAdminIndexView, self).create_form()
        )

    def edit_form(self, obj):
        return self._use_filtered_parent(
            super(MyAdminIndexView, self).edit_form(obj)
        )

    def _use_filtered_parent(self, form):
        #form.pets.query_factory = self._get_parent_list
        return form

    #def _get_parent_list(self):
        # only show available pets in the form
   #     return Pet.query.filter_by(available=True).all()


admin = admin.Admin(app, 'Servicio Registro Empresas', index_view=MyAdminIndexView(), base_template='my_master.html', template_mode='bootstrap3')
