from flask import flash, render_template
from app.models import db
from app.models.archivo_empresas import ArchivoEmpresas
from app.models.company_info import CompanyInfo
from app.models.region import Region
from .admin import admin
from .my_model_view import MyModelView
from flask_admin import form, expose
from flask_admin.babel import gettext, ngettext, lazy_gettext
from flask_admin.actions import action
from wtforms import ValidationError
from werkzeug.datastructures import FileStorage
from base64 import b64encode, b64decode
import datetime
import csv
import re
import os
try:
    from wtforms.fields.core import _unset_value as unset_value
except ImportError:
    from wtforms.utils import unset_value
import flask_login as login


# Fields
class FileUploadB64(form.FileUploadField):
    """
        Customizable file-upload field.

        Saves file to field binary, handles updates and deletions. Inherits from `StringField`,
        resulting filename will be stored as string.
    """
    widget = form.FileUploadInput()

    def __init__(self, label=None, validators=None,
                 column_name=None, namegen=None, allowed_extensions=None,
                 **kwargs):
        """
            Constructor.

            :param label:
                Display label
            :param validators:
                Validators
            :param field_name:
                Column where name is set
            :param namegen:
                Function that will generate filename from the model and uploaded file object.
                Please note, that model is "dirty" model object, before it was committed to database.

                For example::

                    import os.path as op

                    def prefix_name(obj, file_data):
                        parts = op.splitext(file_data.filename)
                        return secure_filename('file-%s%s' % parts)

                    class MyForm(BaseForm):
                        upload = FileUploadField('File', namegen=prefix_name)

            :param allowed_extensions:
                List of allowed extensions. If not provided, will allow any file.
        """
        self.column_name = column_name or 'name'

        self.namegen = namegen or form.namegen_filename
        self.allowed_extensions = allowed_extensions

        super(FileUploadB64, self).__init__(label, validators, **kwargs)

    def is_file_allowed(self, filename):
        """
            Check if file extension is allowed.

            :param filename:
                File name to check
        """
        if not self.allowed_extensions:
            return True

        return ('.' in filename and
                filename.rsplit('.', 1)[1].lower() in
                map(lambda x: x.lower(), self.allowed_extensions))

    def _is_uploaded_file(self, data):
        return (data and isinstance(data, FileStorage) and data.filename)

    def pre_validate(self, form):
        if self._is_uploaded_file(self.data) and not self.is_file_allowed(self.data.filename):
            raise ValidationError(gettext('Invalid file extension'))

        # Handle overwriting existing content
        if not self._is_uploaded_file(self.data):
            return

    def process(self, formdata, data=unset_value):
        if formdata:
            marker = '_%s-delete' % self.name
            if marker in formdata:
                self._should_delete = True

        return super(FileUploadB64, self).process(formdata, data)

    def process_formdata(self, valuelist):
        if self._should_delete:
            self.data = None
        elif valuelist:
            for data in valuelist:
                if self._is_uploaded_file(data):
                    self.data = data
                    break

    def generate_name(self, obj, file_data):
        filename = self.namegen(obj, file_data)
        return filename

    def populate_obj(self, obj, name):
        if self._is_uploaded_file(self.data):
            filename = self.generate_name(obj, self.data)
            self.data.filename = filename

            setattr(obj, name, b64encode(self.data.read()))
            setattr(obj, self.column_name, getattr(obj, self.column_name, None) or filename)


class FileView(MyModelView):

    def is_accessible(self):
        return login.current_user.is_authenticated and login.current_user.has_role('admin')

    # Override form field to use Flask-Admin FileUploadField
    column_list = ['name', 'date', 'state']
    form_create_rules = ('file_content', 'name')
    form_edit_rules = ('name', 'state', 'date')

    form_overrides = {
        'file_content': FileUploadB64
    }

    form_args = {
        'file_content': {
            'label': 'File',
            'column_name': 'name'
        }
    }

    def process_csv(self, s):
        s2 = re.sub('(?<=[0-9]{7}-[0-9K])(;)(?=\w)', ';|', s)
        arch = re.sub('(?<=[A-Z. ",)->¨+0-9;])(;)(?!;)(?=[0-9]*;)', '|;', s2)
        all_lines = arch.splitlines()
        datos = csv.reader(all_lines, csv.excel, delimiter=';', quotechar='|')
        i = 0
        total_datos = len(all_lines)
        ant_time = datetime.datetime.now()
        delta_time = 0
        time_left = datetime.datetime.now()
        per = 0
        for row in datos:
            per = round(((i*100)/total_datos), 2)
            delta_time = datetime.datetime.now() - ant_time
            time_left = datetime.datetime.now() + (delta_time *(total_datos-i))
            ant_time = datetime.datetime.now()
            print("%s %s  %s" %(str(i), str(per), time_left.strftime("%Y-%m-%d %H:%M:%S")))
            print(row)
            if i == 0:
                cabecera = [
                                'rut', 'name', 'numero_resol',
                                'fecha_resol', 'dte_email', 'url'
                           ]
                i += 1
                continue
            j = 0
            empresa = db.session.query(CompanyInfo).filter(CompanyInfo.rut==row[0]).first() or CompanyInfo()
            actualizar = False
            for key in cabecera:
                val = row[j]
                if key == 'region':
                    regiones = db.session.query(Region).filter(Region.prefijo==row[j]).all()
                    val = None
                if key in ['name']:
                    val = row[j].title()
                if key in ['fecha_resol', 'fecha_auth']:
                    val = datetime.datetime.strptime(row[j].replace(' ', ''), "%d-%m-%Y").date()
                if getattr(empresa, key) != val:
                    actualizar = True
                    setattr(empresa, key, val)
                j += 1
            if actualizar:
                empresa.actualizado = datetime.datetime.now()
            if not empresa.id:
                db.session.add(empresa)
            i += 1

    @action('process',
            lazy_gettext('Process File'),
            lazy_gettext('Are you sure you want to process selected records?'))
    def process_file(self, ids):
        try:
            for pk in ids:
                model = self.get_one(pk)
                s = b64decode(model.file_content).decode('iso-8859-1')
                self.process_csv(s)
                model.state = 'done'
            db.session.commit()
            flash('Record was successfully processed.', 'success')
        except Exception as ex:
            flash(gettext('Failed to process records. %(error)s', error=str(ex)), 'error')


    @expose('/process_local')
    def process_local(self):
        csvs = []
        print("yesr %s" % os.path.realpath(os.path.dirname(__file__)))
        for root, dirs, files in os.walk("%s/../data_imp" %os.path.realpath(os.path.dirname(__file__))):
            print("yesr %s" %dirs)
            csvs = files
        while csvs:
            arch = open('%s/../data_imp/%s' % (os.path.realpath(os.path.dirname(__file__)), csvs[0]), 'rb').read().decode('iso-8859-1')
            self.process_csv(arch)
            db.session.commit()
            del(csvs[0])

        return render_template('index.html')


admin.add_view(FileView(ArchivoEmpresas, db.session))
