from app.models import db
from app.models.medio_pago import MedioPago
from .admin import admin
from .my_model_view import MyModelView
import flask_login as login


class MedioPagoAdmin(MyModelView):

    def is_accessible(self):
        return login.current_user.is_authenticated and login.current_user.has_role('admin')


admin.add_view(MedioPagoAdmin(MedioPago, db.session, category="Other"))
