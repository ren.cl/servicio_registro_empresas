from app.models import db
from app.models.user import User
from app.models.user_info import UserInfo
from .my_model_view import MyModelView
from .admin import admin
import flask_login as login


class UserAdmin(MyModelView):

    def is_accessible(self):
        return login.current_user.is_authenticated and login.current_user.has_role('admin')

    form_excluded_columns = ('date', 'token')
    column_exclude_list = ('date', 'password')


admin.add_view(UserAdmin(User, db.session))
admin.add_view(MyModelView(UserInfo, db.session, category="Other"))
