from app.models import db
from app.models.tag import Tag
from .admin import admin
from .my_model_view import MyModelView
import flask_login as login


class TagAdmin(MyModelView):

    def is_accessible(self):
        return login.current_user.is_authenticated and login.current_user.has_role('admin')


admin.add_view(TagAdmin(Tag, db.session))
