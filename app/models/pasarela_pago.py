from . import db


class PasarelaPago(db.Model):
    id = db.Column(db.Integer, primary_key=True)

    name = db.Column(db.String(64), nullable=False)
    code = db.Column(db.String(64))
    active = db.Column(db.Boolean, default=True)
    state = db.Column(db.String(10), default="test")
    fees = db.Column(db.Float)

    medios_pago = db.relationship("MedioPago", back_populates="pasarela")

    def __str__(self):
        return "{}".format(self.name)
