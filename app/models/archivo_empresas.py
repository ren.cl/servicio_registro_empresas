from app.models import db
import datetime


class ArchivoEmpresas(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.Unicode(64))
    file_content = db.Column(db.LargeBinary())
    date = db.Column(db.DateTime, default=datetime.datetime.now())
    state = db.Column(db.Unicode(10), default="draft")

    def __unicode__(self):
        return self.name
