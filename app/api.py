from flask import request
from flask_restful import Resource, Api, abort, reqparse
from app import app
from app.models import db
from app.models.company_info import CompanyInfo
from app.models.company_info_remote import CompanyInfoRemote
from app.models.user import User
from app.models.acteco import Acteco
import re
import datetime
from base64 import b64decode

api = Api(app)

parser = reqparse.RequestParser()
parser.add_argument('token')
parser.add_argument('rut')
parser.add_argument('actualizado')


class CompanyData(Resource):

    def _query_user(self, token):
        if not token:
            abort(403, message="Usuario No autorizado")
        user = db.session.query(User).filter(User.token==token).first()
        if not user:
            abort(403, message="Usuario No registrado")
        if not user.active and user.login != 'public':
            abort(403, message="Usuario desactivado")
        if user.token_usos != -1 and user.token_uso > user.token_usos:
            abort(403, message="Usuario ha consumido todas las consultas disponibles")
        return user

    def get(self):
        args = parser.parse_args()
        token = args.get('token')
        rut = args.get('rut')
        actualizado = args.get('actualizado', False)
        if rut in [False, 0, '0'] or not actualizado:
            abort(403, message="Inconsistencia en datos")
        user = self._query_user(token)
        if 'False' == actualizado:
            actualizado = datetime.datetime.strptime('20190101000000', '%Y%m%d%H%M%S')
        rut = re.sub('^0', '', rut.replace('.', '').upper())
        if db.session.query(CompanyInfo).filter(CompanyInfo.rut==rut, CompanyInfo.actualizado>=actualizado).first():
            return {'result': True}
        return {'result': False}

    def post(self):
        json_data = request.get_json(force=True)
        token = json_data.get('token', False)
        rut = json_data.get('rut', False)
        if rut in [False, 0, '0']:
            abort(403, message="Inconsistencia en datos")
        user = self._query_user(token)
        user.token_uso += 1
        rut = re.sub('^0', '', rut.replace('.', '').upper())
        empresa = db.session.query(CompanyInfo).filter(CompanyInfo.rut==rut).first()
        if not empresa:
            return {}
        result = {
                    'razon_social': empresa.name,
                    'rut': empresa.rut,
                    'dte_email': empresa.dte_email,
                    'fecha_resol': empresa.fecha_resol.strftime("%d-%m-%Y"),
                    'numero_resol': empresa.numero_resol,
                    'actecos': [acteco.code for acteco in empresa.acteco_ids],
                    'url': empresa.url,
                    'actualizado': empresa.actualizado.strftime("%Y-%m-%d"),
                    'glosa_giro': empresa.glosa_giro,
                }
        if user.login != 'public':
            result.update({
                'direccion': empresa.direccion,
                'comuna': empresa.comuna.code if empresa.comuna else False,
                'ciudad': empresa.ciudad.name if empresa.ciudad else False,
                'provincia': empresa.provincia.code if empresa.provincia else False,
                'region': empresa.region.code if empresa.region else False,
                'telefono': empresa.telefono,
                'email': empresa.email,
                'tags': [tag.name for tag in empresa.tag_ids] if empresa.tag_ids else [],
                'logo': empresa.logo,
            })
        empresa.consultas += 1
        db.session.commit()
        return result

    def put(self):
        json_data = request.get_json(force=True)
        token = json_data['token']
        rut = json_data.get('rut', False)
        if rut in [False, 0, '0']:
            abort(403, message="Inconsistencia en datos")
       # user = self._query_user(token)
        rut = re.sub('^0', '', rut.replace('.', '').upper())
        empresa = db.session.query(CompanyInfoRemote).filter(CompanyInfoRemote.rut==rut, CompanyInfoRemote.origen==json_data.get('origen', 'anonimo')).first() or CompanyInfoRemote()
        for key, val in json_data.items():
            if key == 'token' or not val:
                continue
            if key == 'razon_social':
                key = 'name'
            if key == 'actecos':
                if val:
                    val = db.session.query(Acteco).filter(Acteco.code.in_(val)).all()
                key = 'acteco_ids'
            if key == 'logo':
                try:
                    b64decode(val)
                    val = val.encode()
                except:
                    continue
            if not hasattr(empresa, key):
                print("no existe key %s" % key)
                continue
            if key == 'rut':
                val = rut
            setattr(empresa, key, val)
        empresa.actualizado = datetime.datetime.now()
        if not empresa.id:
            db.session.add(empresa)
        db.session.commit()


api.add_resource(CompanyData, '/api/company_info')
